import React from 'react';
import { Redirect } from 'react-router-dom';
import useAuth from './../../hooks/useAuth';

const AuthGuard = ({ children }) => {
  const { isAuth } = useAuth();

  if (!isAuth) {
    return <Redirect to="/login" />;
  }
  return <>{children}</>;
};

export default AuthGuard;
