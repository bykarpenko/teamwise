import { ReactComponent as LogoImg } from './../../assets/logo.svg';
import './style.scss';

const LoadingScreen = () => {
  return (
    <div className="loadingScreen">
      <div className="loading">
        <LogoImg />
        Loading...
      </div>
    </div>
  );
};

export default LoadingScreen;
