import ReactDOM from 'react-dom';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import ItemStatistics from '../ItemStatistics';
import ModalBoard from './ModalBoard';
import { ReactComponent as UserImg } from '../../assets/user.svg';
import './style.scss';

const ItemBoard = ({ item }) => {
  const history = useHistory();
  const [toggleModal, setToggleModal] = useState(false);

  return (
    <>
      <div className="containerItemBoard">
        <div className="bannerItemBoard">
          <div className="headerBannerItemBoard">
            <div className="titleBannerItemBoard">Mood Board</div>
            {/* <div className="subtitleBannerItemBoard">!!!!!!</div> */}
          </div>
          <img className="coverBannerItemBoard" src={item?.image?.url} alt="" />
          <div className="authorItemBoard">
            <div
              className="authorNameItemBoard"
              onClick={() => {
                history.push(`/profile/${item.owner.link}`);
              }}
            >
              {item?.owner?.firstName} {item?.owner?.lastName}
            </div>
            {item?.owner?.avatar ? (
              <img
                className="authorAvatarItemBoard"
                src={item.owner.avatar}
                alt=""
                onClick={() => {
                  history.push(`/profile/${item.owner.link}`);
                }}
              />
            ) : (
              <div
                onClick={() => {
                  history.push(`/profile/${item.owner.link}`);
                }}
                className="userImgItemBoard"
              >
                <UserImg />
              </div>
            )}
          </div>
        </div>
        <div className="infoItemBoard">
          <div
            className="buttonItemBoard"
            onClick={() => setToggleModal(prev => !prev)}
          >
            Watch
          </div>
          <div className="statisticsItemBoard">
            <ItemStatistics item={item} />
          </div>
        </div>
      </div>
      {toggleModal &&
        ReactDOM.createPortal(
          <ModalBoard item={item} setToggleModal={setToggleModal} />,
          document.getElementById('itemModal'),
        )}
    </>
  );
};

export default ItemBoard;
