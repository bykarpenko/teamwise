import ItemStatistics from '../../ItemStatistics';
import { ReactComponent as ImgClose } from '../../../assets/close.svg';
import './style.scss';

const ModalBoard = ({ setToggleModal, item }) => {
  return (
    <div className="wrapperModalBoard">
      <div className="containerModalBoard">
        <div className="headerModalBoard">
          <div className="nameModalBoard">
            <div className="titleModalBoard">Mood Board</div>
            {/* <div className="subtitleModalBoard">!!!!!!!!!</div> */}
          </div>

          <div className="authorModalBoard">
            <div className="authorNameModalBoard">
              {item?.owner?.firstName} {item?.owner?.lastName}
            </div>
            <img
              className="authorAvatarModalBoard"
              src={item?.owner?.avatar}
              alt=""
            />
          </div>
        </div>

        <div
          className={
            item?.description ? 'bodyModalBoard active' : 'bodyModalBoard'
          }
        >
          <img className="imgBannerModalBoard" src={item?.image?.url} alt="" />

          {item?.description && (
            <div className="descriptionModalBoard">{item.description}</div>
          )}
        </div>

        <div className="infoModalBoard">
          <div className="statisticsModalBoard">
            <ItemStatistics item={item} />
          </div>
        </div>

        <div
          className="closeModalBoard"
          onClick={() => setToggleModal(prev => !prev)}
        >
          {' '}
          <ImgClose />{' '}
        </div>
      </div>
    </div>
  );
};

export default ModalBoard;
