import './style.scss';

const OpenAll = ({ title, qtyall }) => {
  return (
    <div className="wrapperOpenAll">
      <div className="containerOpenAll">
        <div className="openAllTitle">{title}</div>
        <div className="openAllBtn">All {qtyall} </div>
        <div className="bgOpenAllImg">bg</div>
      </div>
    </div>
  );
};

export default OpenAll;
