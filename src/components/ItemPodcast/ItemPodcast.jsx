import { useState, useEffect, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import ItemStatistics from '../ItemStatistics';
import { ReactComponent as ImgClose } from '../../assets/close.svg';
import { ReactComponent as ImgForward } from '../../assets/forward15sPodcast.svg';
import { ReactComponent as ImgBack } from '../../assets/back15sPodcast.svg';
import { ReactComponent as ImgPause } from '../../assets/pausePodcast.svg';
import { ReactComponent as ImgPlay } from '../../assets/playPodcast.svg';
import { ReactComponent as UserImg } from '../../assets/user.svg';
// import PodcastAvatarImg from '../../assets/podcastAvatar.png';
import './style.scss';

const ItemPodcast = ({ item }) => {
  const history = useHistory();

  const [toggleAudioPlayer, setToggleAudioPlayer] = useState(true);
  const [Play, setPlay] = useState(false);

  const audio = useRef();
  const progress = useRef();

  const [currentMin, setCurrentMin] = useState(0);
  const [currentSec, setCurrentSec] = useState(0);
  const [durationMin, setDurationMin] = useState(0);
  const [durationSec, setDurationSec] = useState(0);
  const [duration, setDuration] = useState();

  useEffect(() => {
    const sound = audio.current;
    if (!sound) return;
    const durationSetter = () => {
      setDuration(sound.duration);
    };
    // console.log({sound, audio, progress})
    sound.addEventListener('canplay', durationSetter);
    sound.addEventListener('timeupdate', currentTime);

    return () => {
      sound.removeEventListener('canplay', durationSetter);
      sound.removeEventListener('timeupdate', currentTime);
    };
  }, []);

  const currentTime = () => {
    if (!progress.current) return;
    progress.current.value = audio.current.currentTime;
    const min = progress.current.min;
    const max = progress.current.max;
    let value = audio.current.currentTime;
    if (audio.current.duration / 4 > audio.current.currentTime) {
      value = audio.current.currentTime + 1.2;
    } else if (audio.current.duration / 2 > audio.current.currentTime) {
      value = audio.current.currentTime + 1;
    } else if (audio.current.duration / 2 < audio.current.currentTime) {
      value = audio.current.currentTime - 0.9;
    } else if (
      audio.current.duration / 4 + audio.current.duration / 2 <
      audio.current.currentTime
    ) {
      value = audio.current.currentTime - 1;
    }
    // console.log('progress', progress.current);
    progress.current.style.background = `linear-gradient(to right, rgba(255, 255, 255, 1), rgba(255, 255, 255, 1) ${
      ((value - min) / (max - min)) * 100
    }%, rgba(255, 255, 255, 0.3) ${
      ((value - min) / (max - min)) * 100
    }%,rgba(255, 255, 255, 0.3) 100%)`;
    // #DEE2E6

    let mins = Math.floor(audio.current.currentTime / 60);
    let sec = Math.floor(audio.current.currentTime - mins * 60);
    let durmin = Math.floor(audio.current.duration / 60);
    let durSec = Math.floor(audio.current.duration - durmin * 60);
    setCurrentMin(mins);
    setCurrentSec(sec);
    setDurationMin(durmin);
    setDurationSec(durSec);
  };

  const setCurTimePlus = () => {
    audio.current.currentTime += 15;
  };
  const setCurTimeMinus = () => {
    audio.current.currentTime -= 15;
  };

  const onChangeProgressLine = e => {
    audio.current.currentTime = e.target.value;
  };

  const onStartStop = () => {
    !Play ? audio.current.play() : audio.current.pause();
    setPlay(!Play);
  };

  return (
    <div className="containerItemPodcast">
      <audio ref={audio} id="audioPlayer" preload="metadata" controls>
        <source src={item.audio.url} type="audio/mpeg" />
      </audio>
      <div className="bannerItemPodcast">
        {/* <img className="CoverBannerItemPodcast" src={item?.preview?.url || PodcastAvatarImg } alt="" />  */}
        {/* {item?.preview?.url &&  */}
        <img
          className="CoverBannerItemPodcast"
          src={item?.preview?.url}
          alt=""
        />
        {/* } */}
        {toggleAudioPlayer ? (
          <>
            <div
              className="titleBannerItemPodcast"
              onClick={() => {
                // audio.current.play();
                // setPlay(true);
                setToggleAudioPlayer(prev => !prev);
              }}
            >
              Listen to Podcast
            </div>
            {item?.language && (
              <img className="flagBannerItemPodcast" src={item.flag} alt="" />
            )}
            <div className="authorItemPodcast">
              <div
                className="authorNameItemPodcast"
                onClick={() => {
                  history.push(`/profile/${item.owner.link}`);
                }}
              >
                {item?.owner?.firstName} {item?.owner?.lastName}
              </div>
              {item?.owner?.avatar ? (
                <img
                  className="authorAvatarItemPodcast"
                  src={item.owner.avatar}
                  alt=""
                  onClick={() => {
                    history.push(`/profile/${item.owner.link}`);
                  }}
                />
              ) : (
                <UserImg />
              )}
            </div>
          </>
        ) : (
          <div className="audioPlayerItemPodcast">
            <div className="titleAudioPlayerItemPodcast"> Podcast</div>
            <div
              className="closeAudioPlayerItemPodcast"
              onClick={() => {
                audio.current.pause();
                setPlay(false);
                setToggleAudioPlayer(prev => !prev);
                audio.current.currentTime = 0;
                setCurrentMin(0);
                setCurrentSec(0);
              }}
            >
              {' '}
              <ImgClose />{' '}
            </div>
            <div className="navAudioPlayerItemPodcast">
              <ImgBack onClick={setCurTimeMinus} />
              {!Play && <ImgPlay onClick={onStartStop} />}{' '}
              {Play && <ImgPause onClick={onStartStop} />}
              <ImgForward onClick={setCurTimePlus} />
            </div>

            <div className="blockProgressAudioPlayerItemPodcast">
              <div className="timeAudioPlayerItemPodcast">
                <p>{`${currentMin < 10 ? '0' + currentMin : currentMin}:${
                  currentSec < 10 ? '0' + currentSec : currentSec
                }`}</p>
                <p>{`${durationMin < 10 ? '0' + durationMin : durationMin}:${
                  durationSec < 10 ? '0' + durationSec : durationSec
                }`}</p>
              </div>

              <input
                className="progressAudioPlayerItemPodcast"
                ref={progress}
                onChange={onChangeProgressLine}
                type="range"
                min="0"
                max={duration}
                step="0.1"
              />
            </div>
          </div>
        )}
      </div>

      <div className="infoItemPodcast">
        <div className="titleItemPodcast">{item.title}</div>
        <div className="descriptionItemPodcast">{item.description}</div>

        {!!item?.guests?.length && (
          <>
            <div className="titleItemPodcast">Guest</div>
            <div className="guestsItemPodcast">
              {item?.guests?.map((guests, index) => (
                <div
                  className="itemGuestItemPodcast"
                  key={index}
                  onClick={() => {
                    history.push(`/profile/${guests?.link}`);
                  }}
                >
                  {`${guests?.firstName} ${guests?.lastName}`}{' '}
                </div>
              ))}
            </div>
          </>
        )}

        {item?.language && <div className="titleItemPodcast">Language</div>}
        <div className="descriptionItemPodcast">{item.language}</div>
        <div className="statisticsItemPodcast">
          <ItemStatistics item={item} />
        </div>
      </div>
    </div>
  );
};

export default ItemPodcast;
