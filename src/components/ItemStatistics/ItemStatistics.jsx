import { ReactComponent as StatRepostImg } from '../../assets/statrepost.svg';
import { ReactComponent as StatVievsImg } from '../../assets/statvievs.svg';
import { ReactComponent as StatLikesImg } from '../../assets/statlikes.svg';
import './style.scss';

const ItemStatistics = ({ item }) => {
  return (
    <div className="wrapperItemStatistics">
      <div className="containerItemStatistics">
        <div className="ItemRepost">
          <StatRepostImg />
        </div>
        <div className="blockStatistics">
          <div className="itemStatistics">
            <StatVievsImg />
            <div className="qtyItemStatistics">{item.view}</div>
          </div>
          <div className="itemStatistics">
            <StatLikesImg />
            <div className="qtyItemStatistics">{item.like}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ItemStatistics;
