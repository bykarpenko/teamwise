import ReactDOM from 'react-dom';
import { useHistory } from 'react-router-dom';
import { useState } from 'react';
import ItemStatistics from '../ItemStatistics';
import ModalArticle from './ModalArticle';
import { ReactComponent as UserImg } from '../../assets/user.svg';
import './style.scss';

const ItemArticle = ({ item }) => {
  const [toggleModal, setToggleModal] = useState(false);

  const history = useHistory();


  return (
    <>
      <div className="containerItemArticle">
        <div className="bannerItemArticle">
          <img
            className="CoverBannerItemArticle"
            src={item?.preview?.url}
            alt=""
          />
          <div
            className="titleBannerItemArticle"
            onClick={() => setToggleModal(prev => !prev)}
          >
            Read Article
          </div>
          <div className="authorItemArticle">
            {item?.owner?.avatar ? (
              <img
                className="authorAvatarItemArticle"
                src={item.owner.avatar}
                alt=""
                onClick={() => {
                  history.push(`/profile/${item.owner.link}`);
                }}
              />
            ) : (
              <UserImg />
            )}
            <div
              className="authorNameItemArticle"
              onClick={() => {
                history.push(`/profile/${item.owner.link}`);
              }}
            >
              {item?.owner?.firstName} {item?.owner?.lastName}
            </div>
          </div>
        </div>
        <div className="infoItemArticle">
          <div className="titleItemArticle">{item.title}</div>
          <div className="descriptionItemArticle">{item.description}</div>
          <div className="statisticsItemArticle">
            <ItemStatistics item={item} />
          </div>
        </div>
      </div>
      {toggleModal &&
        ReactDOM.createPortal(
          <ModalArticle item={item} setToggleModal={setToggleModal} />,
          document.getElementById('itemModal'),
        )}
    </>
  );
};

export default ItemArticle;
