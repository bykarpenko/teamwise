import AWS from 'aws-sdk';
import { AWS as config } from '../config';

AWS.config.update(config);
const s3 = new AWS.S3({
  params: { Bucket: 'teamwise.22k' },
  region: 'eu-central-1',
});

const s3Uploader = async (file, path) => {
  return new Promise((res, rej) => {
    const params = {
      ACL: 'public-read',
      Body: file,
      Bucket: 'teamwise.22k',
      Key: `${path}/${file.name}`,
    };

    s3.upload(params, (error, data) => {
      if (error) {
        rej(new Error(error.message));
      } else {
        res(data);
      }
    });
  });
};

export default s3Uploader;
