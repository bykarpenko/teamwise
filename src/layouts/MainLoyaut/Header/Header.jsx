import { Link } from 'react-router-dom';
import { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useState } from 'react';
import useAuth from '../../../hooks/useAuth';
import useMessenger from '../../../hooks/useMesenger';
import { ReactComponent as LogoImg } from '../../../assets/logo.svg';
import { ReactComponent as ReminderImg } from '../../../assets/headerIconReminder.svg';
import { ReactComponent as SetttingImg } from '../../../assets/headerIconSetting.svg';
import { ReactComponent as NotificationImg } from '../../../assets/headerIconNotification.svg';
import { ReactComponent as MessageImg } from '../../../assets/headerIconMassage.svg';
import { ReactComponent as TMImg } from '../../../assets/headerIconTM.svg';
import { ReactComponent as UserImg } from '../../../assets/user.svg';
import { ReactComponent as SearchImg } from '../../../assets/search.svg';
import HeaderItemUserDropdown from './HeaderItemUserDropdown';
import './style.scss';

const Header = () => {
  const history = useHistory();

  const { isAuth, user } = useAuth();
  const { threads } = useMessenger();
  const [qtyMessages, setQtyMessages] = useState(null);

  useEffect(() => {
    const allmessage = threads
      .map(item => {
        return item.messages;
      })
      .flat();
    const res = allmessage.filter(
      item => item.sender._id !== user._id && item.isRead !== true,
    ).length;
    setQtyMessages(res);
  }, [threads, user]);

  const [dropdown, setDropdown] = useState(false);

  return (
    <div className="wrapperHeader">
      <div className="conteinerHeader">
        <div
          className={
            dropdown ? 'overlayHeaderDropdown active' : 'overlayHeaderDropdown'
          }
          onClick={() => setDropdown(prev => !prev)}
        />

        <div className="headerBlock">
          <div className="headerLogo">
            <div className="logoTeamwise">
              <LogoImg
                onClick={() => {
                  history.push('/');
                }}
              />
            </div>
            <div
              className="nameHeader"
              onClick={() => {
                history.push('/');
              }}
            >
              TeamWise
            </div>
          </div>

          {isAuth ? (
            <div className="headerAuthInteractions">
              <div className="itemInteraction">
                <div className="qtyItemInteraction">?</div>
                <ReminderImg />
              </div>
              <div className="itemInteraction">
                <SetttingImg />
              </div>
            </div>
          ) : (
            <></>
          )}
        </div>

        <div className="headerSearch">
          <div className="headerSearchImg">
            {' '}
            <SearchImg />{' '}
          </div>
          <input type="text" />
        </div>

        <div className="headerBlock">
          {isAuth ? (
            <div className="headerAuthInteractions">
              <div className="itemInteraction">
                <NotificationImg />
              </div>

              <div
                className="itemInteraction"
                onClick={() => {
                  history.push('/messenger');
                }}
              >
                {!qtyMessages <= 0 ? (
                  <div className="qtyItemInteraction">
                    {qtyMessages <= 9 ? qtyMessages : '9+'}
                  </div>
                ) : (
                  <></>
                )}
                <MessageImg />
              </div>

              <div className="itemInteraction">
                <TMImg />
              </div>
            </div>
          ) : (
            <div className="headerNav">
              <Link to="/login"> Log in</Link>
              <Link to="/signup"> Sign up free</Link>
            </div>
          )}
          <div className="itemUserHeader">
            <div
              className={
                isAuth ? 'bgItemUserHeader active' : 'bgItemUserHeader '
              }
              onClick={() => setDropdown(prev => !prev)}
            >
              {user?.avatar ? <img src={user.avatar} alt="" /> : <UserImg />}
            </div>
            {isAuth ? (
              <div
                className={
                  dropdown
                    ? 'headerItemUserDropdown '
                    : 'headerItemUserDropdown active'
                }
              >
                <HeaderItemUserDropdown user={user} />
              </div>
            ) : (
              <></>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
