import useAuth from '../../../../hooks/useAuth';
import { useHistory } from 'react-router-dom';
import { ReactComponent as SetttingImg } from '../../../../assets/headerIconSetting.svg';
import { ReactComponent as UserImg } from '../../../../assets/user.svg';
import './style.scss';

const HeaderItemUserDropdown = ({ user }) => {
  const history = useHistory();

  const { logout } = useAuth();

  return (
    <div className="containerHeaderItemUserDropdown">
      <div className="userHeaderDropdown">
        <div className="userNameHeaderDropdown">userName</div>
        <div className="bgIconUserHeaderDropdown">
          <UserImg />
        </div>
      </div>
      <div className="settingsHeaderDropdown">
        <SetttingImg />
        <div>Settings</div>
      </div>
      <div
        className="itemHeaderDropdown"
        onClick={() => {
          history.push(`/profile/${user?.link}`);
        }}
      >
        {' '}
        <div>My Profile</div>
      </div>
      <div className="itemHeaderDropdown">
        <div>Followers</div>
      </div>
      <div className="itemHeaderDropdown">
        <div>Favorits</div>
      </div>

      <div className="blockLinkHeaderDropdown">
        <div className="headerBlockLinkHeaderDropdown">
          <div className="titleLinkHeaderDropdown">My invitation link</div>
          <div className="copyLinkHeaderDropdown">Copy </div>
        </div>
        <input
          className="toLinkHeaderDropdown"
          value="teamwiseapp.com/marthasteward"
          disabled
        />
      </div>

      <div className="changePlanHeaderDropdown">
        <div>Change plan</div>
      </div>
      <div
        className="btnLogoutAccountDropdown"
        onClick={() => {
          logout();
        }}
      >
        Log out
      </div>
    </div>
  );
};

export default HeaderItemUserDropdown;
