import Footer from './Footer';
import Header from './Header';
import './style.scss';

const MainLoyaut = ({ children }) => {
  return (
    <>
      {' '}
      <Header />
      <>{children}</>
      <Footer />
    </>
  );
};

export default MainLoyaut;
