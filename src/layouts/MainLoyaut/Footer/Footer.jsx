import { ReactComponent as Telegram } from '../../../assets/telegram.svg';
import { ReactComponent as Facebook } from '../../../assets/facebook.svg';
import { ReactComponent as Instagram } from '../../../assets/instagram.svg';
import { ReactComponent as Linkedin } from '../../../assets/linkedin.svg';
import { ReactComponent as Twitter } from '../../../assets/twitter.svg';
import { ReactComponent as Youtube } from '../../../assets/youtube.svg';
import { ReactComponent as Vimeo } from '../../../assets/vimeo.svg';
import { ReactComponent as Whatsapp } from '../../../assets/whatsapp.svg';
import { ReactComponent as Messenger } from '../../../assets/messenger.svg';
import './style.scss';

const Footer = () => {
  return (
    <div className="wrapperFooter">
      <div className="containerFooter">
        <div className="boxFooter">
          <div className="boxFooterTitle">Help</div>
          <div className="boxFooterItem">FAQ</div>
          <div className="boxFooterItem">Tutorials</div>
          <div className="boxFooterItem">
            <a href="https://www.youtube.com/">
              <Youtube />
            </a>
            <a href="https://www.youtube.com/">youtube/teamwise</a>
          </div>
          <div className="boxFooterItem">
            <a href="https://vimeo.com/">
              <Vimeo />
            </a>
            <a href="https://vimeo.com/">vimeo/teamwise</a>
          </div>
          <div className="boxFooterItem">
            <a href="https://web.telegram.org/k/">
              <Telegram />
            </a>
            <a href="https://web.telegram.org/k/">t.me/teamwise</a>
          </div>
          <div className="boxFooterItem">Pricing</div>
        </div>
        <div className="boxFooter">
          <div className="boxFooterTitle">Contacts</div>
          <div className="boxFooterItem">Social media</div>
          <div className="boxFooterItem">
            <a href="https://www.instagram.com/">
              <Instagram />
            </a>
            <a href="https://www.youtube.com/">
              <Youtube />{' '}
            </a>
            <a href="https://www.linkedin.com/">
              <Linkedin />
            </a>
            <a href="https://twitter.com/?lang=ru">
              <Twitter />
            </a>
            <a href="https://www.facebook.com/">
              <Facebook />
            </a>
          </div>
          <div className="boxFooterItem">Tech support</div>
          <div className="boxFooterItem">
            <a href="https://mail.google.com/">
              <Messenger />
            </a>
            <a href="https://mail.google.com/">info@teamwiseapp.com</a>
          </div>
          <div className="boxFooterItem">
            <a href="https://www.whatsapp.com/">
              <Whatsapp />
            </a>
            <a href="https://www.whatsapp.com/">whatsapp/teamwise</a>
          </div>
          <div className="boxFooterItem">
            <a href="https://web.telegram.org/k/">
              <Telegram />
            </a>
            <a href="https://web.telegram.org/k/">t.me/teamwise</a>
          </div>
        </div>

        <div className="boxFooter">
          <div className="boxFooterTitle">Legal</div>
          <div className="boxFooterItem">Privacy Policy</div>
          <div className="boxFooterItem">{`Terms & conditions`}</div>
          <div className="boxFooterItem">License agreement</div>
          <div className="boxFooterItem">Copyright information</div>
          <div className="boxFooterItem">Cookies policy</div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
