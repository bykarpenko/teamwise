import { useContext } from 'react';
import MessengerContext from '../contexts/MessengerContaxt';

const useMessenger = () => useContext(MessengerContext);

export default useMessenger;
