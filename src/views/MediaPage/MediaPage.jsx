import { useEffect, useState } from 'react';
import axios from './../../utils/axios';
import { url } from './../../config';
import Boards from './../MainPage/Boards';
import VBoards from './../MainPage/VBoards';
import Articles from './../MainPage/Articles';
import ListenToPodcast from './../MainPage/ListenToPodcast';
import LoadingScreen from '../../components/LoadingSreen/LoadingSreen';

import './style.scss';

const MediaPage = () => {
  const [moods, setMoods] = useState(null);
  const [articles, setArticles] = useState(null);
  const [videos, setVideos] = useState(null);
  const [podcasts, setPodcasts] = useState(null);

  useEffect(() => {
    const fetch = async () => {
      const [p1, p2, p3, p4] = await Promise.all([
        axios.get(`${url}/api/media?type=mood&limit=2`),
        axios.get(`${url}/api/media?type=article&limit=2`),
        axios.get(`${url}/api/media?type=video&limit=2`),
        axios.get(`${url}/api/media?type=podcast&limit=2`),
      ]);

      setMoods(p1.data.body);
      setArticles(p2.data.body);
      setVideos(p3.data.body);
      setPodcasts(p4.data.body);
    };

    fetch();
  }, []);

  if (!podcasts || !articles || !moods || !videos) return <LoadingScreen />;

  return (
    <div className="wrapperMediaPage">
      {moods && <Boards moods={moods} />}
      {articles && <Articles articles={articles} />}
      {videos && <VBoards videos={videos} />}
      {podcasts && <ListenToPodcast podcasts={podcasts} />}
    </div>
  );
};

export default MediaPage;
