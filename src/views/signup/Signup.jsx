import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { url } from './../../config';
import s3Uploader from '../../utils/s3Uploader';
import { useState } from 'react';
import { ReactComponent as GoogleSignUpLogo } from '../../assets/googlesignup.svg';
import { ReactComponent as Arrow } from '../../assets/arrow.svg';
import { ReactComponent as TakePhoto } from '../../assets/takePhoto.svg';
import { ReactComponent as UploadPhoto } from '../../assets/uploadPhoto.svg';
import './style.scss';

const Signup = () => {
  const [step, setStep] = useState(1);

  const [data, setData] = useState({
    type: 'Guest',
  });
  const changeData = (filed, value) => {
    if (filed === 'type') {
      setData(prev => ({
        [filed]: value,
        email: prev.email,
        password: prev.password,
      }));
    } else {
      setData(prev => ({
        ...prev,
        [filed]: value,
      }));
    }
  };

  const sendEmail = e => {
    e.preventDefault();

    if (data.password?.length < 8) {
      alert('Need more characters to flog');
      return;
    }

    if (data.password !== data.confirm) {
      alert('Do not match Passwords');
      return;
    }

    axios
      .post(`${url}/api/auth/check-email`, {
        email: data.email,
        password: data.password,
      })
      .then(res => {
        setStep(prev => prev + 1);
        // console.log('good');
      })
      .catch(err => {
        alert(err.message);
        // console.log(`${data.email} already in use`);
      });
  };

  const registration = async e => {
    e.preventDefault();
    if (!checkFinish()) {
      return;
    }

    if (data.avatar) {
      const avatar = await s3Uploader(data.avatar, `${data.email}/avatar`);
      console.log({ avatar });
      data.avatar = avatar.Location;
    }

    axios
      .post(`${url}/api/auth/registration`, data)
      .then(res => {
        history.push('/login');
        alert('confirm email');
      })
      .catch(err => {
        alert('error');
        console.log(err);
      });
  };

  const checkFinish = () => {
    switch (data.type) {
      case 'Guest': {
        let result = true;

        ['firstName', 'lastName'].forEach(key => {
          if (!data[key]) {
            result = false;
          }
        });

        return result;
      }

      case 'Company': {
        let result = true;

        ['companyName', 'dateOfEstablishment'].forEach(key => {
          if (!data[key]) {
            result = false;
          }
        });

        return result;
      }

      default: {
        let result = true;

        ['firstName', 'lastName', 'birth'].forEach(key => {
          if (!data[key]) {
            result = false;
          }
        });

        return result;
      }
    }
  };

  const history = useHistory();

  return (
    <div className="wrapperSignup">
      <div className="containerSignup">
        <div className="iteamTab">
          <div
            className="btnIteamTab"
            onClick={() => {
              history.push('/login');
            }}
          >
            Log in
          </div>
          <div className="btnIteamTabActive">Sign up</div>
        </div>

        {step === 1 && (
          <form className="iteamFormEmail" onSubmit={sendEmail}>
            <div className="signupGoogle">
              <GoogleSignUpLogo /> <div>Sign up with Google</div>
            </div>

            <div className="or">or</div>

            <div className="inputWrapper">
              <input
                type="email"
                value={data.email || ''}
                placeholder="email"
                onChange={e => changeData('email', e.target.value)}
                required
              />
            </div>

            <>
              <div className="inputWrapperNotePassword">
                <input
                  type="password"
                  value={data.password || ''}
                  placeholder="password"
                  onChange={e => changeData('password', e.target.value)}
                  required
                />
              </div>

              <div className="notePassword">
                *Password must content minimum 8 symbols
              </div>
            </>

            <div className="inputWrapper">
              <input
                type="password"
                value={data.confirm || ''}
                placeholder="confirm password"
                onChange={e => changeData('confirm', e.target.value)}
                required
              />
            </div>

            <button
              className={
                data.password?.length >= 1 && data.confirm?.length >= 1
                  ? 'btnNext  active'
                  : 'btnNext'
              }
            >
              Next
            </button>
          </form>
        )}

        {step === 2 && (
          <>
            <form className="itemFormCheckCategory">
              <label>Choose your category:</label>
              <div className="selectCateoryList">
                <div className="selectArrow">
                  <Arrow />
                </div>
                <select onChange={e => changeData('type', e.target.value)}>
                  <option value="Guest">Guest</option>
                  <option value="Architecture & Design">
                    Architecture & Design
                  </option>
                  <option value="Contractor">Contractor</option>
                  <option value="Company">Company</option>
                  <option value="Customer">Customer</option>
                </select>
              </div>
              <div className="labelSettings">Settings</div>
            </form>

            <form className="iteamFormCategory" onSubmit={registration}>
              {[
                'Guest',
                'Architecture & Design',
                'Contractor',
                'Customer',
              ].includes(data.type) && (
                <div className="inputWrapper">
                  <input
                    type="text"
                    value={data.firstName || ''}
                    placeholder="first name"
                    onChange={e => changeData('firstName', e.target.value)}
                    required
                  />
                </div>
              )}

              {[
                'Guest',
                'Architecture & Design',
                'Contractor',
                'Customer',
              ].includes(data.type) && (
                <div className="inputWrapper">
                  <input
                    type="text"
                    value={data.lastName || ''}
                    placeholder="last name"
                    onChange={e => changeData('lastName', e.target.value)}
                    required
                  />
                </div>
              )}

              {['Company'].includes(data.type) && (
                <div className="inputWrapper">
                  <input
                    type="text"
                    value={data.companyName || ''}
                    placeholder="company name"
                    onChange={e => changeData('companyName', e.target.value)}
                    required
                  />
                </div>
              )}

              {[
                'Company',
                'Architecture & Design',
                'Contractor',
                'Customer',
              ].includes(data.type) && (
                <div className="inputWrapper">
                  <input
                    type="text"
                    value={data.address?.zip || ''}
                    placeholder="zip"
                    onChange={e =>
                      changeData('address', {
                        ...data.address,
                        zip: e.target.value,
                      })
                    }
                    required
                  />
                </div>
              )}

              {[
                'Company',
                'Architecture & Design',
                'Contractor',
                'Customer',
              ].includes(data.type) && (
                <div className="inputWrapper">
                  <input
                    type="text"
                    value={data.address?.country || ''}
                    placeholder="country"
                    onChange={e =>
                      changeData('address', {
                        ...data.address,
                        country: e.target.value,
                      })
                    }
                    required
                  />
                </div>
              )}

              {[
                'Company',
                'Architecture & Design',
                'Contractor',
                'Customer',
              ].includes(data.type) && (
                <div className="inputWrapper">
                  <input
                    type="text"
                    value={data.address?.province || ''}
                    placeholder="state/county/province"
                    onChange={e =>
                      changeData('address', {
                        ...data.address,
                        province: e.target.value,
                      })
                    }
                    required
                  />
                </div>
              )}

              {['Company'].includes(data.type) && (
                <div className="inputWrapper">
                  <input
                    type="date"
                    value={data.dateOfEstablishment || ''}
                    placeholder="date of Establishment"
                    onChange={e =>
                      changeData('dateOfEstablishment', e.target.value)
                    }
                    required
                  />
                </div>
              )}
              {['Architecture & Design', 'Contractor', 'Customer'].includes(
                data.type,
              ) && (
                <div className="inputWrapper">
                  <input
                    type="date"
                    value={data.birth || ''}
                    placeholder="birth date"
                    onChange={e => changeData('birth', e.target.value)}
                    required
                  />
                </div>
              )}

              {[
                'Guest',
                'Company',
                'Architecture & Design',
                'Contractor',
                'Customer',
              ].includes(data.type) && (
                <div className="inputWrapperAvatar">
                  {data.avatar && (
                    <img src={URL.createObjectURL(data.avatar)} alt="avatar" />
                  )}
                  <input
                    type="file"
                    id="avatarRegistration"
                    onChange={e => {
                      if (!e.target.files[0]) return;
                      changeData('avatar', e.target.files[0]);
                    }}
                  />
                  {!data.avatar && (
                    <div
                      className="titleInputWrapperAvatar"
                      htmlFor="avatarRegistration"
                    >
                      {' '}
                      add avatar{' '}
                    </div>
                  )}
                  <div className="blockIconsInputAvatar">
                    <label
                      className="bgItemIconInputAvatar"
                      htmlFor="avatarRegistration"
                    >
                      <UploadPhoto />
                    </label>
                    <label className="bgItemIconInputAvatar">
                      <TakePhoto />
                    </label>
                  </div>
                </div>
              )}

              <button
                className={checkFinish() ? 'btnFinish  active' : 'btnFinish'}
              >
                Finish
              </button>
            </form>
          </>
        )}
      </div>
    </div>
  );
};

export default Signup;
