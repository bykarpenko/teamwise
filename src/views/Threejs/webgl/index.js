import * as THREE from 'three';
import { Box3, Vector3 } from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import Soldier from './../../../assets/Soldier.glb';

export default class WebGL {
  constructor(container) {
    this.container = container;
    this.clock = new THREE.Clock();
    this.meshList = [];
    this.init();
  }

  init() {
    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color(0xf1f1f1);
    this.camera = new THREE.PerspectiveCamera(
      40,
      this.container.offsetWidth / this.container.offsetHeight,
      0.1,
      1000,
    );
    this.camera.position.set(4, 4, 4);
    this.camera.lookAt(0, 0, 0);
    this.renderer = new THREE.WebGLRenderer({ antialias: true });
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(
      this.container.offsetWidth,
      this.container.offsetHeight,
    );
    this.renderer.toneMapping = THREE.ACESFilmicToneMapping;
    this.renderer.toneMappingExposure = 1.0;
    this.renderer.outputEncoding = THREE.sRGBEncoding;

    this.container.appendChild(this.renderer.domElement);

    this.initControls();
    this.initLight();
    this.animate();
    window.addEventListener('resize', this.resize.bind(this));
    this.loaderThree();
  }

  loaderThree() {
    const loader = new GLTFLoader();

    loader.load(Soldier, gltf => {
      this.scene.add(gltf.scene);
      console.log('gltf.scene', gltf.scene);
      gltf.userData = new Box3().setFromObject(gltf.scene);
      gltf.scene.position.y -= gltf.userData.getSize(new Vector3()).y / 2;

      const animations = gltf.animations;
      this.mixer = new THREE.AnimationMixer(gltf.scene);

      this.runAction = this.mixer.clipAction(animations[1]);
      this.idleAction = this.mixer.clipAction(animations[0]);
      this.walkAction = this.mixer.clipAction(animations[3]);
    });
  }

  actionAnimation(value) {
    this.runAction.stop();
    this.idleAction.stop();
    this.walkAction.stop();
    this[value].play();
  }

  building(qty) {
    const geometryBox = new THREE.BoxGeometry(0.9, 0.9, 0.9);
    const geometryCylinder = new THREE.CylinderGeometry(0.9, 0.9, 0.9, 16);
    const geometrySphere = new THREE.SphereGeometry(0.9, 32, 16);

    const array = [geometryBox, geometryCylinder, geometrySphere];

    for (let a = -qty+1; a <= qty-1; a += 2) {
      for (let h = -qty+1; h <= qty-1; h += 2) {
        for (let i = -qty+1; i <= qty-1; i += 2) {
          const randomColor = Math.floor(Math.random() * 16777215).toString(16);
          const material = new THREE.MeshStandardMaterial({
            color: parseInt('0x' + randomColor, 16),
          });

          const cube = new THREE.Mesh(array[this.random()], material);

          cube.position.set(a, h, i);
          this.scene.add(cube);
          this.meshList.push(cube);
        }
      }
    }
  }

  qtyFigures(qty) {
    console.log(this.scene);

    this.meshList.forEach(item => {
      item.geometry.dispose();
      item.material.dispose();
      item.parent.remove(item);
    });

    this.meshList = [];
    this.building(qty);
  }

  random(n = 2) {
    return Math.round(Math.random() * n);
  }

  initControls() {
    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
  }

  initLight() {

    this.directionalLightFirst = new THREE.DirectionalLight(0xd2f3ff, 1);
    this.directionalLightFirst.position.set(200, 100, 200);
    this.scene.add(this.directionalLightFirst);

    this.directionalLightSecond = new THREE.DirectionalLight(0xd2f3ff, 0.75);
    this.directionalLightSecond.name = 'directionalLightSecond';
    this.directionalLightSecond.position.set(-0, 100, -150);
    this.scene.add(this.directionalLightSecond);

    this.ambientLight = new THREE.AmbientLight('white', 1);
    this.ambientLight.position.set(-200, 100, 150);
    this.scene.add(this.ambientLight);
  }

  resize() {
    this.camera.aspect =
      this.container.offsetWidth / this.container.offsetHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(
      this.container.offsetWidth,
      this.container.offsetHeight,
    );
  }

  animate() {
    requestAnimationFrame(() => {
      this.animate();
    });
    this.renderer.render(this.scene, this.camera);

    let mixerUpdateDelta = this.clock.getDelta();
    this.mixer?.update(mixerUpdateDelta);

    // this.controls.update();
  }
}
