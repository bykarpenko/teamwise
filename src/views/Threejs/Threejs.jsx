import WebGL from './webgl';
import { useEffect, useRef } from 'react';
import { useState } from 'react';

import './style.scss';

const Threejs = () => {
  const ref = useRef();
  const webglRef = useRef();

  const [qty, setQty] = useState('');

  const sendqty = e => {
    e.preventDefault();
    webglRef.current.qtyFigures(qty);
  };

  const sendanimation = value => {
    webglRef.current.actionAnimation(value);
  };

  useEffect(() => {
    webglRef.current = new WebGL(ref.current);
    const current = ref.current;
    return () => {
      if (current && WebGL.current) {
        current.removeChild(WebGL.current.renderer.domElement);
      }
    };
  }, []);

  return (
    <>
      <div className="wrapperThreejs" ref={ref}></div>

      <form className="blockQtyFigures" onSubmit={sendqty} action="">
        <div>enter qty figures</div>
        <input
          className="inputQtyFigures"
          onChange={e => setQty(e.target.value)}
          type="number"
        />
        <button className="buttonQtyFigures">Enter</button>
      </form>

      <div className="blockAnimations">
        <div
          className="btnAnimation"
          onClick={() => sendanimation('idleAction')}
        >
          idle
        </div>
        <div
          className="btnAnimation"
          onClick={() => sendanimation('walkAction')}
        >
          walk
        </div>
        <div
          className="btnAnimation"
          onClick={() => sendanimation('runAction')}
        >
          run
        </div>
      </div>
    </>
  );
};

export default Threejs;
