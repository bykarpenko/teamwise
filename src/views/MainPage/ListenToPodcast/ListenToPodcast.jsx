import ReactDOM from 'react-dom';
import { useState } from 'react';
import ModalAllPodcast from '../../../components/ItemPodcast/ModalAllPodcast';
import ItemPodcast from '../../../components/ItemPodcast';

import './style.scss';

const ListenToPodcast = ({ podcasts, qty = 2, userPager }) => {
  const [toggleModalAll, setToggleModalAll] = useState(false);

  return (
    <>
      <div className="wrapperListenToPodcast">
        <div className="containerListenToPodcast">
          {podcasts.slice(0, qty).map(item => (
            <div key={item._id} className="wrapperItemPodcast">
              <ItemPodcast item={item} />
            </div>
          ))}
          <div
            className="MainBtnMore"
            onClick={() => setToggleModalAll(prev => !prev)}
          >
            More
          </div>
        </div>
      </div>
      {toggleModalAll &&
        ReactDOM.createPortal(
          <ModalAllPodcast
            qty={podcasts.length}
            userPager={userPager}
            userPodcast={podcasts}
            toggleModalAll={toggleModalAll}
            setToggleModalAll={setToggleModalAll}
          />,
          document.getElementById('itemModal'),
        )}
    </>
  );
};

export default ListenToPodcast;
