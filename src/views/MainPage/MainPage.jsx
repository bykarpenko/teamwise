import { useEffect, useState } from 'react';
import axios from './../../utils/axios';
import { url } from './../../config';
import Carousel from './Carousel';
import Projects from './Projects';
import Boards from './Boards';
import VBoards from './VBoards';
import Professionals from './Professionals';
import Articles from './Articles';
import ListenToPodcast from './ListenToPodcast';
import Watch from './Watch';

import LoadingScreen from '../../components/LoadingSreen/LoadingSreen';

const MainPage = () => {
  const [dataMainPage, setDataMainPage] = useState(null);
  

  useEffect(() => {
    axios
      .get(`${url}/api/main-page`)
      .then(res => {
        setDataMainPage(res.data.body);
        console.log(res.data.body);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  if (!dataMainPage) return <LoadingScreen />;

  return (
    <div className="wrapperMainPage">
      <Carousel />
      <Watch />
      {dataMainPage?.['Recommended projects'] && (
        <Projects
          projects={dataMainPage?.['Recommended projects']}
          title="Recomended projects"
        />
      )}
      {dataMainPage?.['Mood boards'] && (
        <Boards moods={dataMainPage?.['Mood boards']} />
      )}
      {dataMainPage?.['Top professionals'] && (
        <Professionals
          professionals={dataMainPage?.['Top professionals']}
          title="Top Professionals for you"
        />
      )}
      {dataMainPage?.Videos && <VBoards videos={dataMainPage?.Videos} />}
      {dataMainPage?.['Popular projects'] && (
        <Projects
          projects={dataMainPage?.['Popular projects']}
          title="Popular projects"
        />
      )}
      {dataMainPage?.Articles && <Articles articles={dataMainPage?.Articles} />}
      {dataMainPage?.['Latest projects'] && (
        <Projects
          projects={dataMainPage?.['Latest projects']}
          title="Latest projects"
        />
      )}
      {dataMainPage?.Podcasts && (
        <ListenToPodcast podcasts={dataMainPage?.Podcasts} />
      )}
    </div>
  );
};

export default MainPage;
