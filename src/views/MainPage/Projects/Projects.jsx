import OpenAll from '../../../components/OpenAll';
import ItemProject from '../../../components/ItemProject';

import './style.scss';

const Projects = ({ title, projects, qty = 3 }) => {
  const qtyall = projects?.length;

  return (
    <div className="wrapperProjects">
      <div className="containerProjects">
        <OpenAll title={title} qtyall={qtyall} />
        {projects.slice(0, qty).map(item => (
          <div key={item._id} className="wrapperItemProject">
            <ItemProject item={item} />
          </div>
        ))}
      </div>
    </div>
  );
};

export default Projects;
