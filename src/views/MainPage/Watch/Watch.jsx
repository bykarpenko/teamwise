import './style.scss';

const Watch = () => {
  return (
    <div className="wrapperWatch">
      <div className="containerWatch">
        <div className="itemWatch">
          <img src="https://picsum.photos/511/354" alt="" />
          <div className="itemWatchText">
            <h1>Dreamers</h1>
            <p>Build the house of your dreams</p>
          </div>
          <div className="itemWatchBtn">Watch</div>
        </div>
        <div className="itemWatch">
          <img src="https://picsum.photos/536/358" alt="" />
          <div className="itemWatchText">
            <h1>Designers</h1>
            <p>Create your best projects ever</p>
          </div>
          <div className="itemWatchBtn">Watch</div>
        </div>
        <div className="itemWatch">
          <img src="https://picsum.photos/536/359" alt="" />
          <div className="itemWatchText">
            <h1>Contractors</h1>
            <p>Land the most awesome jobs</p>
          </div>
          <div className="itemWatchBtn">Watch</div>
        </div>
        <div className="itemWatch">
          <img src="https://picsum.photos/536/344" alt="" />
          <div className="itemWatchText">
            <h1>Companies</h1>
            <p>Manage everything in your browser</p>
          </div>
          <div className="itemWatchBtn">Watch</div>
        </div>
      </div>
    </div>
  );
};

export default Watch;
