import { useHistory } from 'react-router-dom';
import useAuth from '../../hooks/useAuth';
import { useState } from 'react';
import { ReactComponent as GoogleSignUpLogo } from '../../assets/googlesignup.svg';
import './style.scss';

const Login = () => {
  const history = useHistory();
  const handleHistory = () => {
    history.push('/signup');
  };

  const { login } = useAuth();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [remember, setRemember] = useState(false);

  const send = async e => {
    e.preventDefault();
    try {
      await login(email, password, remember);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="wrapperLogin">
      <div className="containerLogin">
        <div className="iteamTab">
          <div className="btnIteamTabActive">Log in</div>
          <div
            className="btnIteamTab"
            onClick={() => {
              handleHistory();
            }}
          >
            Sign up
          </div>
        </div>

        <form className="iteamForm" onSubmit={send}>
          <div className="signupGoogle">
            <GoogleSignUpLogo /> <div>Sign up with Google</div>
          </div>

          <div className="or">or</div>

          <div className="inputWrapper">
            <input
              value={email}
              onChange={e => setEmail(e.target.value)}
              placeholder="email"
              type="email"
              name="name"
              required
            />
          </div>

          <div className="inputWrapper">
            <input
              value={password}
              onChange={e => setPassword(e.target.value)}
              placeholder="password"
              type="password"
              name="name"
              required
            />
          </div>

          <label className="checkboxItem">
            Remember me
            <input
              type="checkbox"
              value={remember}
              onClick={() => setRemember(prev => !prev)}
            />
            <span className="checkmark"></span>
          </label>

          <button className="btnLogin">Log in</button>

          <div className="forgotPassword">I forgot my password</div>
        </form>
      </div>
    </div>
  );
};

export default Login;
