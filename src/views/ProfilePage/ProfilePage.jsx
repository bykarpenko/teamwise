import { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import axios from './../../utils/axios';
import { url } from './../../config';
import { Link } from 'react-scroll';
import Boards from './../MainPage/Boards';
import VBoards from './../MainPage/VBoards';
import Professionals from './../MainPage/Professionals';
import Articles from './../MainPage/Articles';
import ListenToPodcast from './../MainPage/ListenToPodcast';
import Projects from './../MainPage/Projects';
import ProfileUser from './ProfileUser';

import LoadingScreen from '../../components/LoadingSreen/LoadingSreen';
import './style.scss';

const ProfilePage = () => {
  const history = useHistory();

  const { link } = useParams();

  const [user, setUser] = useState(null);

  useEffect(() => {
    axios
      .get(`${url}/api/user/${link}`)
      .then(res => {
        console.log(res.data.body);
        setUser(res.data.body);
        console.log(res.data.body);
      })
      .catch(err => {
        history.push('/');
      });
  }, [link, history]);

  if (!user) return <LoadingScreen />;

  return (
    <div className="wrapperProfilePage">
      <ProfileUser user={user} />
      <div className="wrapperScrollPage">
        <div className="containerScrollPage">
          {!!user?.team?.length && (
            <Link to="team" smooth={true} className="itemScrollPage">
              Team
            </Link>
          )}
          {!!user?.moods?.length && (
            <Link to="moodboards" smooth={true} className="itemScrollPage">
              Moodboards
            </Link>
          )}
          {!!user?.projects?.length && (
            <Link to="projects" smooth={true} className="itemScrollPage">
              Projects
            </Link>
          )}
          {!!user?.videos?.length && (
            <Link to="video" smooth={true} className="itemScrollPage">
              Video
            </Link>
          )}
          {!!user?.articles?.length && (
            <Link to="articles" smooth={true} className="itemScrollPage">
              Articles
            </Link>
          )}
          {!!user?.podcasts?.length && (
            <Link to="podcasts" smooth={true} className="itemScrollPage">
              Podcasts
            </Link>
          )}
        </div>
      </div>
      {!!user?.team?.length && (
        <div id="team">
          <Professionals professionals={user.team} title="Our Team" />
        </div>
      )}
      {!!user?.moods?.length && (
        <div id="moodboards">
          <Boards userPager={true} moods={user.moods} />
        </div>
      )}
      {!!user?.projects?.length && (
        <div id="projects">
          <Projects projects={user.projects} title="Our Projects" />
        </div>
      )}
      {!!user?.videos?.length && (
        <div id="video">
          <VBoards userPager={true} videos={user.videos} />
        </div>
      )}
      {!!user?.articles?.length && (
        <div id="articles">
          <Articles userPager={true} articles={user.articles} />
        </div>
      )}
      {!!user?.podcasts?.length && (
        <div id="podcasts">
          <ListenToPodcast userPager={true} podcasts={user.podcasts} />
        </div>
      )}
    </div>
  );
};

export default ProfilePage;
