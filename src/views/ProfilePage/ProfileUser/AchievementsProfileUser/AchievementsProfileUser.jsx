import { useState } from 'react';
import './style.scss';

const AchievementsProfileUser = ({ user }) => {
  const [btnAchievementsProfileUser, setBtnAchievementsProfileUser] =
    useState(false);
  const [hoverAchievement, setHoverAchievement] = useState('');

  return (
    <>
      <div
        className={
          btnAchievementsProfileUser
            ? 'overlayProfileUser active'
            : 'overlayProfileUser '
        }
        onClick={() => setBtnAchievementsProfileUser(prev => !prev)}
      />

      <div
        className={
          btnAchievementsProfileUser
            ? 'achievementsBlockProfileUser active'
            : 'achievementsBlockProfileUser '
        }
      >
        {new Array(btnAchievementsProfileUser ? 24 : 4)
          .fill(0)
          .map((_, index) => {
            if (user?.awards?.[index]) {
              return (
                <div
                  className={
                    hoverAchievement === index
                      ? 'ItemAchievementProfileUser active'
                      : 'ItemAchievementProfileUser'
                  }
                  key={index}
                >
                  <img
                    className="iconAchievementProfileUser"
                    src={user.awards[index].img}
                    alt=""
                    onMouseEnter={() => setHoverAchievement(index)}
                    onMouseLeave={() => setHoverAchievement('')}
                  />

                  {hoverAchievement === index && (
                    <div className="textAchievementProfileUser">
                      <div className="titleAchievementProfileUser">
                        {user.awards[index].title}
                      </div>
                      <div className="descriptionAchievementProfileUser">
                        {user.awards[index].description}
                      </div>
                      <div className="dateAchievementProfileUser">
                        {' '}
                        {user.awards[index].date}
                      </div>
                    </div>
                  )}
                </div>
              );
            } else {
              return (
                <div className="ItemAchievementProfileUser" key={index}></div>
              );
            }
          })}

        <div
          className={
            user?.awards?.length <= 4
              ? 'bntAchievementsProfileUser disable'
              : 'bntAchievementsProfileUser '
          }
          onClick={() => {
            if (user?.awards?.length <= 4) {
              return;
            }
            setBtnAchievementsProfileUser(prev => !prev);
          }}
        >
          {btnAchievementsProfileUser ? 'x' : 'All'}
        </div>
      </div>
    </>
  );
};

export default AchievementsProfileUser;
