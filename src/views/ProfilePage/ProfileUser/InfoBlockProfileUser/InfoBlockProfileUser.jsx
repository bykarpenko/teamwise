import './style.scss';
import { useState } from 'react';

const InfoBlockProfileUser = ({ user }) => {
  const [btnAreasProfileUser, setBtnAreasProfileUser] = useState(true);
  const [btnServicesProfileUser, setBtnServicesProfileUser] = useState(true);
  const [btnFollowProfileUser, setBtnFollowProfileUser] = useState(false);

  return (
    <>
      <div
        className={
          btnAreasProfileUser
            ? 'overlayProfileUser'
            : 'overlayProfileUser active'
        }
        onClick={() => setBtnAreasProfileUser(prev => !prev)}
      />
      <div
        className={
          btnAreasProfileUser
            ? 'deployedAreasProfileUser active'
            : 'deployedAreasProfileUser '
        }
      >
        <div
          className="btnDeployedAreas"
          onClick={() => setBtnAreasProfileUser(prev => !prev)}
        >
          Close
        </div>
        <div className="textDeployedInfoBlockProfileUser">
          <div className="titleDeployedInfoBlockProfileUser">Serving Areas</div>
          <div className="bodyDeployedInfoBlockProfileUser">
            {user?.areas.map((item, index) => (
              <li key={index}>{item}</li>
            ))}
          </div>
        </div>
      </div>

      <div
        className={
          btnServicesProfileUser
            ? 'overlayProfileUser'
            : 'overlayProfileUser active'
        }
        onClick={() => setBtnServicesProfileUser(prev => !prev)}
      />
      <div
        className={
          btnServicesProfileUser
            ? 'deployedServicesProfileUser active'
            : 'deployedServicesProfileUser '
        }
      >
        <div
          className="btnDeployedServices"
          onClick={() => setBtnServicesProfileUser(prev => !prev)}
        >
          Close
        </div>
        <div className="textDeployedInfoBlockProfileUser">
          <div className="titleDeployedInfoBlockProfileUser">
            Services Provided
          </div>
          <div className="bodyDeployedInfoBlockProfileUser">
            {user?.services.map((item, index) => (
              <li key={index}>{item}</li>
            ))}
          </div>
        </div>
      </div>

      <div className="infoBlockProfileUser">
        {user?.status && (
          <>
            <div className="btnProfileUser"> {user.status} </div>
          </>
        )}
        <div
          className="btnAreasProfileUser"
          onClick={() => setBtnAreasProfileUser(prev => !prev)}
        >
          Areas
        </div>
        <div
          className="btnServicesProfileUser"
          onClick={() => setBtnServicesProfileUser(prev => !prev)}
        >
          Services
        </div>
        <div
          className={
            btnFollowProfileUser
              ? 'btnFollowProfileUser active'
              : 'btnFollowProfileUser'
          }
          onClick={() => setBtnFollowProfileUser(prev => !prev)}
        >
          {btnFollowProfileUser ? 'Unfollow' : 'Follow'}
        </div>
      </div>
    </>
  );
};

export default InfoBlockProfileUser;
