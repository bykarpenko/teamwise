import './App.scss';
import routes, { renderRoutes } from './routes';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { AuthProvider } from './contexts/AuthContext';
import { MessengerProvider } from './contexts/MessengerContaxt';

const history = createBrowserHistory();

const App = () => {
  return (
    <Router history={history}>
      <AuthProvider>
        <MessengerProvider>{renderRoutes(routes)}</MessengerProvider>
      </AuthProvider>
    </Router>
  );
};

export default App;
