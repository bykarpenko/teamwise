import { createContext, useEffect, useReducer } from 'react';
import jwtDecode from 'jwt-decode';
import { url } from '../config';
import axios from '../utils/axios';
import LoadingScreen from './../components/LoadingSreen/LoadingSreen';

const initialState = {
  isAuth: false,
  isInitial: false,
  user: null,
};

const isValidToken = accessToken => {
  if (!accessToken) {
    return false;
  }
  const decoded = jwtDecode(accessToken);
  const currentTime = Date.now() / 1000;

  return decoded.exp > currentTime;
};

const setSession = accessToken => {
  if (accessToken) {
    localStorage.setItem('tokenToken', accessToken);
    axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
  } else {
    localStorage.removeItem('tokenToken');
    delete axios.defaults.headers.common.Authorization;
  }
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'LOGIN': {
      return {
        ...state,
        isAuth: true,
        isInitial: true,
        user: action.payload.user,
      };
    }

    case 'LOGOUT': {
      return {
        ...state,
        isAuth: false,
        user: null,
      };
    }

    case 'INIT': {
      return {
        ...state,
        isInitial: true,
      };
    }

    default: {
      return {
        ...state,
      };
    }
  }
};

const AuthContext = createContext({
  ...initialState,
  login: () => {},
  logout: () => {},
});

export const AuthProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    const initial = async () => {
      try {
        const accessToken = window.localStorage.getItem('tokenToken');

        if (accessToken && isValidToken(accessToken)) {
          setSession(accessToken);

          const res = await axios.get(`${url}/api/user`);

          dispatch({
            type: 'LOGIN',
            payload: {
              user: res.data.body,
            },
          });
        } else {
          dispatch({
            type: 'INIT',
          });
        }
      } catch (err) {
        console.log(err);
        setSession(null);
        dispatch({ type: 'LOGOUT' });
      }
    };
    initial();
  }, []);

  if (!state.isInitial) {
    return <LoadingScreen />;
  }

  const login = async (email, password, remember) => {
    const res = await axios.post(`${url}/api/auth/login`, {
      email,
      password,
      remember,
    });

    const { accessToken, user } = res.data.body;

    setSession(accessToken);

    dispatch({
      type: 'LOGIN',
      payload: {
        user,
      },
    });
  };

  const logout = () => {
    setSession(null);
    dispatch({ type: 'LOGOUT' });
  };

  return (
    <AuthContext.Provider value={{ ...state, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
